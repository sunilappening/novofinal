<?php include('includes/header.php') ?>

<div id="main" class="inner-page account-page help-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 bg-white">
				<?php include('includes/account-nav.php') ?>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">
						<div class="page-header">
							<h3 class="font-lead m-0">Help</h3>
						</div>
						<div class="assessments">
							<div class="row">
								<div class="col">
									<ul class="list-group">
										<li class="list-group-item">
											<span class="font-weight-bold">Cras justo odio</span>
											<span class="float-right"><img src="./assets/images/arrow-right-circle.png" alt=""></span>
										</li>
										<li class="list-group-item">
											<span class="font-weight-bold">Dapibus ac facilisis in</span>
											<span class="float-right"><img src="./assets/images/arrow-right-circle.png" alt=""></span>
										</li>
										<li class="list-group-item">
											<span class="font-weight-bold">Morbi leo risus</span>
											<span class="float-right"><img src="./assets/images/arrow-right-circle.png" alt=""></span>
										</li>
										<li class="list-group-item">
											<span class="font-weight-bold">Porta ac consectetur ac</span>
											<span class="float-right"><img src="./assets/images/arrow-right-circle.png" alt=""></span>
										</li>
										<li class="list-group-item">
											<span class="font-weight-bold">Vestibulum at eros</span>
											<span class="float-right"><img src="./assets/images/arrow-right-circle.png" alt=""></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>