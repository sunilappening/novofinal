<?php include('includes/header.php') ?>

<div id="main" class="inner-page assessment-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 bg-white">
				<div class="inner-page-nav assessment-nav row">
						<div class="inner-page-header assessment-header w-100 p-2">
							<h4 class="text-center">Assessments</h4>
							<div class="inner-page-search assessment-search">
								<input type="text" id="search" class="form-control">
								<label for="search" class="search-label">
									<img src="./assets/images/svg/search-icon.svg" alt="">
									<span>Search</span>
								</span>
							</div>
						</div>

						<div class="inner-page-list assessment-list w-100">
							<div class="li-block active common-assessments">
								<p class="font-weight-bold mb-1">Common Assessment</p>
								<p class="small">Quick Access to common tests</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>
							<div class="li-block">
								<p class="mb-1">USB-1</p>
								<p class="small">Unabbreviated Full Title of Form</span>
							</div>

						</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">
						<div class="page-header">
							<h3 class="font-lead m-0">Common Assessments</h3>
						</div>
						<div class="assessments">
							<div class="row">
								<div class="col-md-6">
									<a href="/single-assessment.php">
									<div class="single-assessment text-center p-5">
										<h3>DASS - 21</h3>
										<p class="font-medium">
											Depression Anxiety Stress Scales - Short Form
										</p>
									</div>
									</a>
								</div>
								<div class="col-md-6">
									<a href="/single-assessment.php">
									<div class="single-assessment text-center p-5">
										<h3>DASS - 21</h3>
										<p class="font-medium">
											Depression Anxiety Stress Scales - Short Form
										</p>
									</div>
									</a>
								</div>
								<div class="col-md-6">
									<a href="/single-assessment.php">
									<div class="single-assessment text-center p-5">
										<h3>DASS - 21</h3>
										<p class="font-medium">
											Depression Anxiety Stress Scales - Short Form
										</p>
									</div>
									</a>
								</div>
								<div class="col-md-6">
									<a href="/single-assessment.php">
									<div class="single-assessment text-center p-5">
										<h3>DASS - 21</h3>
										<p class="font-medium">
											Depression Anxiety Stress Scales - Short Form
										</p>
									</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>