<?php include('includes/header.php') ?>

<div id="main" class="inner-page account-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4 bg-white">
				<?php include('includes/account-nav.php') ?>
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="inner-page-content common-assessment-content w-100">
						<div class="page-header">
							<h3 class="font-lead m-0">Account Details</h3>
						</div>
						<div class="assessments">
							<div class="row">
								<div class="col">
									<form action="">
										<div class="form-group">
											<label for="">Title</label>
											<div class="radio-group">
												<div class="radio-input">
													<input type="radio" id="radio" name="radio">
													<label for="radio">Dr</label>
												</div>
												<div class="radio-input">
													<input type="radio" id="radio2" name="radio" checked="">
													<label for="radio2">Mr</label>
												</div>
												<div class="radio-input">
													<input type="radio" id="radio3" name="radio">
													<label for="radio3">Mrs</label>
												</div>
												<div class="radio-input">
													<input type="radio" id="radio4" name="radio">
													<label for="radio4">Ms</label>
												</div>
												<div class="radio-input">
													<input type="radio" id="radio5" name="radio">
													<label for="radio5">Prof</label>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<div class="form-group">
													<label for="">First Name</label>
													<input type="text" class="form-control">
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<label for="">Last Name</label>
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col">
												<div class="form-group">
													<label for="">Email</label>
													<input type="text" class="form-control">
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<label for="">Profession</label>
													<input type="text" class="form-control">
												</div>
											</div>
										</div>
										<div class="form-group mt-2">
											<button class="btn btn-success">Update Details</button>
										</div>
									</form>
									<hr class="my-4">
									<h4 class="legend">Security</h4>
									<div class="form-group mt-4">
										<button class="btn btn-default mr-2">Change Passcode</button>
										<button class="btn btn-default">Change Password</button>
									</div>
									<hr class="my-4">
									<h4 class="legend">Email Preferences</h4>
									<p>Would you like to receive reports in PDF format?</p>
									<div class="radio-group">
										<div class="radio-input">
											<input type="radio" id="pref" name="pref">
											<label for="pref">No</label>
										</div>
										<div class="radio-input">
											<input type="radio" id="pref1" checked name="pref">
											<label for="pref1">As Link</label>
										</div>
										<div class="radio-input">
											<input type="radio" id="pref2" name="pref">
											<label for="pref2">As Attachment</label>
										</div>
									</div>
									<div class="pad30"></div>
									<p>Would you like to receive reports in CSV format?</p>
									<div class="radio-group">
										<div class="radio-input">
											<input type="radio" id="csv" name="csv">
											<label for="csv">No</label>
										</div>
										<div class="radio-input">
											<input type="radio" id="csv1" checked name="csv">
											<label for="csv1">As Link</label>
										</div>
										<div class="radio-input">
											<input type="radio" id="csv2" name="csv">
											<label for="csv2">As Attachment</label>
										</div>
									</div>
									<div class="form-group mt-4">
										<button class="btn btn-success">Save preferences</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include('includes/navigation.php') ?>